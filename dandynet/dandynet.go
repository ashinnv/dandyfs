package dandynet

import (
	"encoding/gob"
	"fmt"
	"net"

	"blocks"
)

type Index []blocks.Block

//SyncToServer ensures that this client Index is synchronized with a DandyFS server
func (ndx *Index) SyncToServer(address string) bool {
	var incomingIndex Index
	var ssccReturn chan [2]Index = make(chan [2]Index)
	var syncStatusCheckChan chan [2]Index = make(chan [2]Index) //For passing to asynchronus goroutine that checks if the blocks are supposed to be synced in the first place
	var recMxt [][]byte                                         //For pulling down the server's index
	var recLst [][]byte                                         //For sending hashes of requested files to the server
	var bklist [][]byte                                         //Blacklist of file hashes to ignore

	go checkBlackList(syncStatusCheckChan, ssccReturn, bklist)

	for {
		//Initiate a connection to the server
		conn, conErr := net.Dial("tcp", address)
		if conErr != nil {
			fmt.Println("Error dialing server at", address, "Error:", conErr)
			continue
		}

		//The server sends back a list of blocks that it has
		dec := gob.NewDecoder(conn)
		decErr := dec.Decode(&recMxt)
		if decErr != nil {
			fmt.Println("Error running the decode:", decErr)
			continue
		}

		syncDiff := getSyncDiff(*ndx, recMxt)

		//Make sure these bytes aren't blacklisted
		syncStatusCheckChan <- syncDiff
		syncDiff = <-ssccReturn

		//Check for missing indexes. Returns [2]Index. [0] is the list of missing on client, [1] is the list of missing on server
		recLst = recMxtToRecMat(syncDiff)

		//request the files that we want
		enc := gob.NewEncoder(conn)
		encErr := enc.Encode(recLst)

		if encErr != nil {
			fmt.Println("Error encoding for sync-down request:", encErr)
		}

		//Decode incoming diff
		dec = gob.NewDecoder(conn)
		ndxDecErr := dec.Decode(incomingIndex)
		if ndxDecErr != nil {
			fmt.Println("Error decoding for sync-down of file data:", ndxDecErr)
		}

	}
}

//recMxtToRecMat takes the [2]Index input, only keeps the one
//slice for local blocks and returns the file hashes of those blocks
func recMxtToRecMat(input [2]Index) [][]byte {
	sendCheck := input[1]
	retMat := [][]byte{} //Matrix, 2d array?

	for _, iter := range sendCheck {
		retBt := iter.FileHash
		retMat = append(retMat, retBt)
	}

	return retMat
}

//checkBlackList() checks to make sure we don't try to pull down blacklisted blocks
func checkBlackList(input chan [2]Index, output chan [2]Index, blacklist [][]byte) {

	for iter := range input {

		for n, inp := range iter[0] {
			for _, bl := range blacklist {
				if inp == bl {
					remove(&iter, n)
				}
			}
		}

	}
}

func remove(input [][]byte, n int) {

	var inter [][]byte
	for ndx := range input {
		if n == ndx {
			continue
		}

	}
}

//getSyncDiff() looks through the remote index and the local index and
func getSyncDiff(local Index, remote [][]byte) [2]Index {
	found := false
	retIndex := [2]Index{}

	//Get local uniqure Blocks
	for _, ndx1 := range local {
		for _, ndx2 := range remote {

			if string(ndx1.FileHash) == string(ndx2) {
				found = true
				break
			}

		}
		if found {
			continue
		}

		retIndex[0] = append(retIndex[0], ndx1)
	}

	//Get remote unique blocks
	for _, ndx1 := range remote {
		for _, ndx2 := range local {
			if string(ndx1) == string(ndx2.FileHash) {
				found = true
				break
			}
		}
		if found {
			continue
		}
		retIndex[1] = append(retIndex[1], ndx1)
	}

	return retIndex
}

/*
//getSyncDiff returns a slice of block pointners that are missing
func _getSyncDiff(local Index, remote Index) Index {
	var big Index
	var lil Index
	var localMissing Index //Indexs that are not on the client
	var servrMissing Index //Indexes that are not on the server

	if len(local) > len(remote) {
		big = local
		lil = remote
	} else {
		lil = local
		big = remote
	}

	//sort the slices?

	//So we can break out of the two layers of loops
	doubleBreaker := false
	//look for matches and diffs in the slices
	for _, itr := range big {
		for _, lilItr := range lil {

			//Check for matching indexes
			if string(itr.FileHash) == string(lilItr.FileHash) {
				//Found a match, stop looking
				doubleBreaker = true
				break
			}

		}

		//If we've found a match between the two indexes, skip adding it to the diffs and move on to the next index
		if doubleBreaker == true {
			doubleBreaker = false
			continue
		} else {

		}
	}

	return
}
*/
