package blocks

import (
	"fmt"
	"io/ioutil"
	"path"
	"time"
)

type Block struct {
	NDX      uint64
	FileHash []byte //Hash of file

	Bytes        []byte
	Name         string
	Dir          string //Absolute directory of client location of file
	CreationDate time.Time
	EditDates    map[time.Time]string //map[time edited]username
	FileExt      string

	Classes  []string       //Classifier tags for image/video recognition, etc...
	Histgram map[string]int //Histogram data for images and video

	LocalSync bool //Whether or not the file is present on the client
	ClientID  string
	ServerID  string
}

// SPIKE SPIKE SPIKE verifyLoc() makes sure the input string is a real file and is a legitimate location.
func verifyLoc(input string) bool {
	return true
}

func GenerateBlock(location string, prev uint64) (Block, error) {

	var retBlk Block = Block{}
	if !verifyLoc(location) {
		locerr := fmt.Errorf("location not verified")
		return retBlk, locerr
	}

	bts, rderr := ioutil.ReadFile(location)
	if rderr != nil {
		fmt.Println("Error reading:", rderr)
		return retBlk, rderr
	}

	retBlk.NDX = prev + 1
	retBlk.Bytes = bts
	retBlk.Name = path.Base(location)
	retBlk.Dir = location
	retBlk.Classes = []string{}
	retBlk.CreationDate = time.Now()
	retBlk.EditDates = make(map[time.Time]string)
	retBlk.FileExt = path.Ext(location)

	return retBlk, nil
}
